import React, { useState } from "react";
import { useFetch } from './useFetch';
import { useCountRenders } from "./useCountRenders";

export const Hello = React.memo(({ increment }) => {
    const [{ count, count2 }, setCount] = useState({ count: 10, count2: 20 });
    const {data, loading} = useFetch(`http://numbersapi.com/${count}/trivia`);
    useCountRenders();
    return (
        <div>
            <div>{loading ? "loading..." : data}</div> 
            <button onClick={() => setCount(currentState => ({
                ...currentState,
                count: currentState.count + 1,
                count2: currentState.count2 + 10
            }))}>
                +
            </button>
            <div>Count 1: {count}</div>
            <div>Count 2: {count2}</div>
            <button onClick={increment}>useCallback</button>
        </div>
    )
});