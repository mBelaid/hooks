export default function reducer(state, action) {
    switch (action.type) {
      case "add-todo":
        return {
          todos: [...state.todos , { email: action.email, completed: false }],
          todoCount: state.todoCount + 1
        };
      case "toggle-todo":
        return {
          todos: state.todos.map(
              (t, index) => index === action.index ? {...t, completed: !t.completed} : t
          ),
          todoCount: state.todoCount
        };
        default:
          return state;
    }
  }