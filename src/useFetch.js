import { useEffect, useState, useContext } from "react"
import { UserContext } from "./UserContext";

export const useFetch = url => {
    const { value, setValue } = useContext(UserContext);
    const [state, setState] = useState({ data: null, loading: true });
    useEffect(() => {
        fetch(url)
        .then(x => x.text())
        .then(data => {
            setState({ data, loading: false });
            setValue(data);
        });
    }, [url]);
    return state;
}