import React, { useContext, useState, useCallback } from 'react';
import {range, inRange} from 'lodash';
import styled from 'styled-components';
import Draggable from '../Draggable';
import { UserContext } from '../UserContext';

const MAX = 5;
const HEIGHT = 80;

export const About = () => {
    const items = range(MAX);
    const [state, setState] = useState({
        order: items,
        dragOrder: items, // items order while dragging
        draggedIndex: null
    });

    const { value, setValue, todos, todoCount, dispatch } = useContext(UserContext); 

    const handleDrag = useCallback(({translation, id}) => {
        const delta = Math.round(translation.y / HEIGHT);
        const index = state.order.indexOf(id);
        const dragOrder = state.order.filter(index => index !== id);
            
        if (!inRange(index + delta, 0, items.length)) {
          return;
        }
            
        dragOrder.splice(index + delta, 0, id);
            
        setState(state => ({
          ...state,
          draggedIndex: id,
          dragOrder
        }));
      }, [state.order, items.length]);
        
      const handleDragEnd = useCallback(() => {
        setState(state => ({
          ...state,
          order: state.dragOrder,
          draggedIndex: null
        }));
      }, []);
    return (
        <div>
            <h3>{value}</h3>
            <pre>{JSON.stringify(todos)}</pre>
            <Container>
                {items.map(index => {
                    const isDragging = state.draggedIndex === index;
                    const top = state.dragOrder.indexOf(index) * (HEIGHT + 10);
                    const draggedTop = state.order.indexOf(index) * (HEIGHT + 10);
                            
                    return (
                    <Draggable
                        key={index}
                        id={index}
                        onDrag={handleDrag}
                        onDragEnd={handleDragEnd}
                    >
                        <Rect
                        isDragging={isDragging}
                        top={isDragging ? draggedTop : top}
                        >
                        {index}
                        </Rect>
                    </Draggable>
                    );
                })}
            </Container>
        </div>
    )
};

const Container = styled.div`
  width: 100vw;
  min-height: 50vh;
`;

const Rect = styled.div.attrs(props => ({
  style: {
    transition: props.isDragging ? 'none' : 'all 300ms ease-out',
    fontWeight: props.isDragging ? 'bold' : 'normal',
    color: props.isDragging ? 'red' : 'black'
  }
}))`
  width: 300px;
  user-select: none;
  height: ${HEIGHT}px;
  background: #fff;
  box-shadow: 0 5px 10px rgba(${({isDragging}) => isDragging ? '255' : '0'}, 0, 0, 0.15);
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: ${({top}) => 10 + top}px;
  left: calc(50vw - 150px);
  font-size: 20px;
  color: #777;
`;
