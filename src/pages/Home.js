import React, { useState, useEffect, useRef, useCallback, useContext } from 'react';
import { useForm } from '../useForm';
import { Hello } from '../Hello';
import '../App.css';
import { UserContext } from '../UserContext';

export const Home = () => {
  const { value, setValue, todos, todoCount, dispatch } = useContext(UserContext); 
  const [showHello, setShowHello] = useState(true);
  const [values, handleChange] = useForm({ email: "", password: "" });
  const appRef = useRef();
  const inputRef = useRef();
  
  useEffect(() => {
    const onMouseMove = e => {
      if(e.x > 1000) {
        appRef.current.style.background = "pink";
        appRef.current.style.float = "right";
      } else if (e.x < 120) {
        appRef.current.style.background = "yellow";
        appRef.current.style.float = "left";
      } else {
        appRef.current.style.background = "white";
        appRef.current.style.float = 'none';
      }
    }
    window.addEventListener("mousemove", onMouseMove);
    return () => {
      window.removeEventListener("mousemove", onMouseMove);
    };
  }, [values]);

  const increment = useCallback(() => {
    setShowHello(c => !c);
  }, []);

  return (
    <div className="App" ref={appRef}>
      <h3>usecontext: {value}</h3>
      <button onClick={() => setShowHello(!showHello)}>Toggle</button>
      {showHello && <Hello increment={increment} />}
      <button onClick={() => inputRef.current.focus()}>Focus</button>
      <form
        onSubmit={e => {
          e.preventDefault();
          dispatch({ type: "add-todo", email: values.email });
          handleChange({target: {name: "email", value: ""}});
        }}
      >
        <input
          name="email"
          type="email"
          value={values.email}
          onChange={handleChange}
          autoComplete="on"
          ref={inputRef}
        />
        <input
          name="password"
          type="password"
          value={values.password}
          onChange={handleChange}
          autoComplete="off"
        />
        <button type="submit">Submit</button>
      </form>
      <div>Email: {values.email}</div>
      <div>Password: {values.password}</div>
      <h1>TODOS LIST</h1>
      <div>number of todos: {todoCount}</div>
      {todos.map((t, index) =>
        <div
          key={index}
          onClick={() => dispatch({ type: "toggle-todo", index })}
          style={{ cursor: "pointer", textDecoration: t.completed ? "line-through" : "" }}
        >
          {t.email}
        </div>
      )}
    </div>
  );
}