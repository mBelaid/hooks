import { useRef } from "react"

export const useCountRenders = () => {
    const renders = useRef(false);
    console.log("renders: ", !renders.current);
}