import React, { useState, useMemo, useReducer } from 'react';
import { BrowserRouter, Route, Link } from "react-router-dom";

import { Home } from "./pages/Home";
import { About } from "./pages/About";
import { UserContext } from "./UserContext";
import reducer from './reducer';

const App = () => {
  const [{ todos, todoCount }, dispatch] = useReducer(reducer, { todos: [], todoCount: 0 })
  const [value, setValue] = useState("hello from context");
  const ProviderValue = useMemo(
    () => ({value, setValue, todos, todoCount, dispatch}), [value, setValue, todos, todoCount, dispatch]
  );
  return (
    <BrowserRouter>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
          </ul>
        </nav>
        <UserContext.Provider value={ProviderValue}>
          <Route path="/" exact component={Home} />
          <Route path="/about" component={About} />
        </UserContext.Provider>
      </div>
    </BrowserRouter>
  );
}

export default App;
